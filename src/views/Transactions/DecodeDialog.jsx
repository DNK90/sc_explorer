import React, { useState, useEffect } from "react";
import Button from "../../components/CustomButtons/Button";
import {Dialog, DialogActions, DialogContent, DialogTitle} from "@material-ui/core";
import Info from "../../components/Typography/Info";
import TextField from "@material-ui/core/TextField/TextField";
import IconButton from "@material-ui/core/IconButton";
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import Autocomplete from "@material-ui/lab/Autocomplete/Autocomplete";
import Card from "../../components/Card/Card";
import CardBody from "../../components/Card/CardBody";
import Primary from "../../components/Typography/Primary";
import {toDateTime} from "../../state/block";
import {TRANSACTION} from "../../constants";
import GridContainer from "../../components/Grid/GridContainer";
import GridItem from "../../components/Grid/GridItem";
const protobuf = require("protobufjs");

const DecodeDialog = ({ payload }) => {
    const [isOpened, open] = useState(false);
    const [decodedResult, setDecodedResult] = useState({});
    const [protoTypes, setPrototypes] = useState([]);
    const [protoType, setProtoType] = useState({});

    const decodePayload = (payload, protoType) => {
        try{
            let root = protobuf.Root.fromJSON((JSON.parse(localStorage.getItem(TRANSACTION.PROTO))));
            let lookupType = root.lookupType(protoType);
            // convert payload into buffer
            let data = Uint8Array.from(Buffer.from(payload, "hex"));
            setDecodedResult(lookupType.decode(data));
        } catch(error) {
            return setDecodedResult({ error: error.message });
        }
    };

    useEffect(() => {
    }, []);

    return (
        <>
        <Button onClick={() => {
            open(true);
        }} size="sm" color="success">Decode</Button>
        <Dialog open={ isOpened } fullWidth={ true } maxWidth="lg" onClose={() => { open(false) }} disableBackdropClick={ true }
                disableEscapeKeyDown={ true }>
            <DialogContent>
                <GridContainer style={{ display: "flex" }}>
                    <GridItem xs={4}>
                        <Info><h3><b>Decode Dialog</b></h3></Info>
                        <div style={{ marginTop: 20 }}>
                            <Info>
                                Import protobuf in *.json format
                                <input
                                    accept="*/*"
                                    id="icon-button-search"
                                    onChange={(evt) => {
                                        const fileReader = new FileReader();
                                        fileReader.readAsText(evt.target.files[0]);
                                        console.log(fileReader.name);
                                        fileReader.onload = (e) => {
                                            try {
                                                const result = JSON.parse(e.target.result);
                                                localStorage.setItem(TRANSACTION.PROTO, e.target.result);
                                                const root = protobuf.Root.fromJSON(result);
                                                let protoTypes = [];
                                                Object.keys(root.nested).forEach(key => {
                                                    if (key !== "google" && key !== "gorm") {
                                                        Object.keys(root.nested[key].nested).forEach(nestedKey => {
                                                            protoTypes.push({
                                                                key: nestedKey,
                                                                value: `${key}.${nestedKey}`
                                                            })
                                                        })
                                                    }
                                                });
                                                setPrototypes(protoTypes);
                                            } catch(err) {
                                                setDecodedResult({ error: "invalid definition" })
                                            }
                                        }
                                    }}
                                    type="file"
                                    style={{ display: 'none' }}
                                />
                                <label htmlFor="icon-button-search">
                                    <IconButton color="primary" component="span">
                                        <CloudUploadIcon />
                                    </IconButton>
                                </label>
                            </Info>
                        </div>
                        <div>
                            <Autocomplete
                                id="protoTypesSelect"
                                options={protoTypes}
                                getOptionLabel={(option) => option.key}
                                style={{ width: 300 }}
                                renderInput={(params) => <TextField {...params} label="choose decode type" variant="outlined" />}
                                onChange={(evt, value)=> {
                                    setProtoType(value);
                                }} />
                        </div>
                        <Button onClick={() => { decodePayload(payload, protoType.value); }} variant="contained" color="info" style={{ marginTop: ".75rem"}}>Decode</Button>
                    </GridItem>
                    <GridItem xs={8}>
                        <Card style={{ height: 530, overflowY: "auto", overflowX: "auto", marginTop: ".75rem"}}>
                            {
                                decodedResult && decodedResult !== {} ?
                                    <CardBody>
                                        {console.log(decodedResult)}
                                        {
                                            Object.keys(decodedResult).map((key, idx) => (
                                                <div key={idx}>
                                                    { console.log(key) }
                                                    <Primary><b>{key}:</b></Primary>
                                                    {typeof decodedResult[key] === "object" ? (decodedResult[key]["seconds"] > 0 ? toDateTime(decodedResult[key]["seconds"]).toUTCString() : JSON.stringify(decodedResult[key])) : decodedResult[key] }
                                                </div>
                                            ))
                                        }
                                    </CardBody>
                                    : ""
                            }
                        </Card>
                    </GridItem>
                </GridContainer>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => { open(false); setDecodedResult({}) }} variant="contained" color="danger" style={{ marginRight: "auto", marginLeft: "auto" }}>Close</Button>
            </DialogActions>
        </Dialog>
        </>
    );
};

export default DecodeDialog;
