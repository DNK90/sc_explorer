import Card from '../../components/Card/Card.jsx';
import CardBody from '../../components/Card/CardBody.jsx';
import Muted from '../../components/Typography/Muted';
import Info from "../../components/Typography/Info";
import React, { useState, useEffect } from "react";
import {compose} from "recompose";
import {connect} from "react-redux";
import {path} from "lodash/fp";
import {EXPLORER_API, TRANSACTION} from "../../constants";
import {lifecycle} from "recompose/dist/Recompose.cjs";
import {
    fetchTransactionsAC,
} from "../../state/transaction";
import {toDateTime} from "../../state/block";
import { withStyles } from '@material-ui/core/styles';
import {List} from "../Dashboard";
import DecodeDialog from "./DecodeDialog";
import axios from "axios";

const hrSpace = {
    marginTop: ".75rem",
    marginBottom: ".75rem",
    opacity: ".75"
};

const useStyles = theme => ({
    card: {
        [theme.breakpoints.up("md")]: {
            width: "800px",
            marginLeft: "auto",
            marginRight: "auto"
        }
    },
    table: {
        [theme.breakpoints.up("xl")]: {
            width: "1920px",
            marginRight: "auto",
            marginLeft: "auto"
        }
    },
    modal: {
        '& .MuiTextField-root': {
            margin: theme.spacing.unit,
            width: '25ch',
        },
    },
    modal_div : {
        marginRight: 15,
        marginLeft: 15
    }
});

const fetchTransactionInfo = async (hash) => {
    try {
        const { data } = await axios.get(`${EXPLORER_API}/api/transaction/${hash}`);
        return data
    } catch (err) {
        return err;
    }
};

const TransactionLayout = ({ classes }) => {
    const [tx, setTx] = useState({});
    useEffect(() => {
        const fetchTx = async () => {
            let { pathname } = window.location;
            if (pathname.indexOf("transaction/") > -1) {
                pathname = pathname.replace("transaction/", "");
            }
            let res = await fetchTransactionInfo(pathname);
            if (res.transactionHash === undefined) window.location.href = `${window.location.origin}/404`;
            setTx(res);
        };
        fetchTx();
    }, []);
    return (
    <div className={classes.card}>
        <Muted><h3><b>Transaction Details</b></h3></Muted>
        <Card>
            <CardBody>
                <Info><b>Transaction Hash</b></Info>
                <Muted>{tx.transactionHash}</Muted>

                <hr style={hrSpace} />

                <Info><b>Block Height</b></Info>
                <Info><a href={`${window.location.origin}/block/${tx.blockHeight}`}>{tx.blockHeight}</a></Info>

                <hr style={hrSpace} />

                <Info><b>Nonce</b></Info>
                <Muted>{tx.nonce ? tx.nonce : 0}</Muted>

                <hr style={hrSpace} />

                <Info><b>Time</b></Info>
                <Muted>{tx.time ? toDateTime(tx.time.seconds - (new Date().getTimezoneOffset() * 60)).toLocaleString() : 0}</Muted>

                <hr style={hrSpace} />

                <Info><b>OrganizationId</b></Info>
                <Info>{ tx.organizationId ? <a href={`${window.location.origin}/organization/${tx.organizationId}`}>{tx.organizationId}</a> : "" }</Info>

                <hr style={hrSpace} />

                <Info><b>Organization Hash</b></Info>
                <Muted>{ tx.idHash ? <a href={`${window.location.origin}/organization/${tx.idHash}`}>{tx.idHash}</a> : "" }</Muted>

                <hr style={hrSpace} />

                <Info><b>Method</b></Info>
                <Muted>{tx.method}</Muted>

                <hr style={hrSpace} />

                <Info><b>Sender</b></Info>
                <Muted><a href={`${window.location.origin}/address/${tx.from}`}>{tx.from}</a></Muted>

                <hr style={hrSpace} />

                <Info><b>To</b></Info>
                <Muted>{tx.to}</Muted>

                <hr style={hrSpace} />

                <Info><b>Payload</b></Info>
                <Muted>{tx.payload}</Muted>
                <DecodeDialog payload={tx.payload} />

                <hr style={hrSpace} />

                <Info><b>Signature</b></Info>
                <Muted>{tx.signature}</Muted>

                <hr style={hrSpace} />

            </CardBody>
        </Card>
    </div>
    )
};

const TransactionsLayout = ({ classes, transactions, getTransactions }) => (
    <div className={classes.table}>
        <List height={700} label={`${transactions.total} Transactions found`} tableHead={["Hash", "Height", "Time", "Organization ID", "Organization Hash", "Method", "From"]} tableData={transactions} tableHeaderColor={"info"} detailsUri="" hasPaging={true} getTableData={getTransactions}/>
    </div>
);
export const Transaction = withStyles(useStyles)(TransactionLayout);
export const Transactions = compose(
    connect(
        state => ({
            transactions: path(TRANSACTION.DOT_TRANSACTIONS_LIST)(state)
        }),
        {
            getTransactions: fetchTransactionsAC.actionCreator
        }
    ),
    withStyles(useStyles),
    lifecycle({
        componentDidMount() {
            this.props.getTransactions();
        }
    })
)(TransactionsLayout);

