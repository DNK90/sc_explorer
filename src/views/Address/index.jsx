import React, { useState, useEffect } from 'react';
import axios from 'axios';
import {EXPLORER_API} from "../../constants";
import {timeSince} from "../../state/block";
import {List} from "../Dashboard";
import withStyles from "@material-ui/core/styles/withStyles";


const useStyles = theme => ({
    breakpoint: {
        [theme.breakpoints.up("lg")]: {
            width: "1280px",
            marginRight: "auto",
            marginLeft: "auto"
        }
    }
});

const fetchTransactions = async (p, l, o) => {

    let { pathname } = window.location;
    if (pathname.indexOf("/address/") > -1) {
        pathname = pathname.replace("/address/", "");
    }
    let { data: { transactions, total, limit, page, order } } = await axios.get(`${EXPLORER_API}/api/address/${pathname}/transactions?limit=${l}&page=${p}&order=${o}`);
    if (transactions === undefined) return {};
    let results = [];
    transactions.forEach(({ transactionHash, blockHeight, time, organizationId, idHash, method }) => {
        results.push([
            {
                urls: [
                    {
                        url: `${window.location.origin}/transaction/${transactionHash}`,
                        text: transactionHash.slice(0, 20) + "...",
                    }
                ]
            },
            organizationId ? {
                urls: [
                    {
                        url: `${window.location.origin}/organization/${organizationId}`,
                        text: organizationId,
                    }
                ]
            } : "",
            idHash ? {
                urls: [
                    {
                        url: `${window.location.origin}/organization/${idHash}`,
                        text: idHash.slice(0, 20) + "...",
                    }
                ]
            } : "",
            {
                urls: [
                    {
                        url: `${window.location.origin}/block/${blockHeight}`,
                        text: blockHeight,
                    }
                ]
            },
            timeSince(time.seconds),
            method,
        ]);
    });
    return {data: results, total: total, limit: limit, page: page !== undefined ? page : 0, order: order !== undefined ? order : 0 };
};

const AddressLayout = ({ classes }) => {

    const [txs, setTxs] = useState({ data: [], limit: 10, page: 0, order: 0 });
    const [address, setAddress] = useState("");

    useEffect(() => {
        const fetchTxs = async () => {
            let res = await fetchTransactions(txs.page, txs.limit, txs.order);
            if (res.data === undefined || res.data.length === 0 ) {
                window.location.href = `${window.location.origin}/404`;
            }
            setTxs(res);
        };
        fetchTxs();

        let { pathname } = window.location;
        if (pathname.indexOf("/address/") > -1) {
            pathname = pathname.replace("/address/", "");
        }
        setAddress(pathname);
    }, []);

    return (
        <div className={classes.breakpoint}>
            {
                txs.data.length > 0 ?
                    <List
                        height={700}
                        label={`Address ${address} - Page #${txs.page+1} (Found ${txs.total} transactions)`}
                        tableHead={["Transaction Hash", "OrganizationID", "Organization Hash", "Height", "Time", "Method"]} tableData={txs} tableHeaderColor={"info"}
                        detailsUri="" getTableData={(page, limit, order) => {
                        const switchPage = async () => {
                            let res = await fetchTransactions(page, limit, order);
                            setTxs(res);
                        };
                        switchPage();
                    }
                    } hasPaging={true}
                    /> : ""
            }
        </div>
    )
};

export const Address = withStyles(useStyles)(AddressLayout);
