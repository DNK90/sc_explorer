import React, { useEffect } from "react";
import Warning from "../../components/Typography/Warning";
import Card from "../../components/Card/Card";
import CardBody from "../../components/Card/CardBody";
import Muted from "../../components/Typography/Muted";
import CardHeader from "../../components/Card/CardHeader";

export const NotFound = () => {

    useEffect(() => {
        // remove previous history
        let history = JSON.parse(localStorage.getItem("history"));
        if (history !== null) {
            if (history.length === 1) {
                localStorage.removeItem("history");
            } else {
                localStorage.setItem("history", JSON.stringify(history.slice(0, history.length-2)));
            }
        }
    }, []);

    return (
        <Card style={{ width: 600, marginRight: "auto", marginLeft: "auto" }}>
            <CardHeader>
                <Warning><h4><b>Page Not Found</b></h4></Warning>
            </CardHeader>
            <CardBody>
                <Muted style={{ fontSize: 11 }}><b>The page you are looking does not exist or data not found.</b></Muted>
            </CardBody>
        </Card>
    )
};
