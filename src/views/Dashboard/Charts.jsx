import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import grey from "@material-ui/core/es/colors/grey";
import {ArgumentAxis, Chart, LineSeries, ValueAxis} from "@devexpress/dx-react-chart-material-ui";
import ReactInterval from "react-interval";
import {compose} from "recompose";
import {connect} from "react-redux";
import { path } from 'lodash/fp';
import {
    BLOCK, TRANSACTION
} from "../../constants";
import {fetchBlocksPerHoursAC, fetchBlocksPerMinAC} from "../../state/block";
import GridContainer from "../../components/Grid/GridContainer";
import GridItem from "../../components/Grid/GridItem";
import {fetchTransactionsPerHoursAC, fetchTransactionsPerMinAC} from "../../state/transaction";

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
    tabsColor: {
        backgroundColor: grey[800]
    },
    chart: {
        paddingRight: '20px',
    },
    title: {
        whiteSpace: 'pre',
    },
    breakpoints: {
        [theme.breakpoints.up("xl")]: {
            width: "1920px",
            marginRight: "auto",
            marginLeft: "auto"
        }
    },
});

const emptyData = [
    {time: 1, count: 0},
    {time: 2, count: 0},
    {time: 3, count: 0},
    {time: 4, count: 0},
    {time: 5, count: 0},
    {time: 6, count: 0},
    {time: 7, count: 0},
    {time: 8, count: 0},
    {time: 9, count: 0},
    {time: 10, count: 0}
];

const TabContainer = (props) => {
    return (
        <Typography component="div" style={{padding: 8 * 3}}>
            {props.children}
        </Typography>
    );
};

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
};

class ChartsLayout extends React.Component {
    state = {
        blockValue: 0,
        txValue: 0
    };

    handleBlockChange = (event, value) => {
        this.setState({blockValue: value});
        console.log(`handleBlockChange ${JSON.stringify(this.state)}`);
    };

    handleTxChange = (event, value) => {
        this.setState({txValue: value});
        console.log(`handleTxChange ${JSON.stringify(this.state)}`);
    };

    render() {
        const {classes} = this.props;
        const {blockValue, txValue} = this.state;

        return (
            <div className={classes.breakpoints}>
                <GridContainer>
                    <GridItem xs={6} sm={6}>
                        <div className={classes.root}>
                            <AppBar position="static">
                                <Tabs className={classes.tabsColor} value={blockValue} onChange={this.handleBlockChange}>
                                    <Tab label="Blocks By Minute"/>
                                    <Tab label="Blocks By Hours"/>
                                </Tabs>
                            </AppBar>
                            {blockValue === 0 && <TabContainer><BlockPerMins/></TabContainer>}
                            {blockValue === 1 && <TabContainer><BlockPerHours/></TabContainer>}
                        </div>
                    </GridItem>
                    <GridItem xs={6} sm={6}>
                        <div className={classes.root}>
                            <AppBar position="static">
                                <Tabs className={classes.tabsColor} value={txValue} onChange={this.handleTxChange}>
                                    <Tab label="Transactions By Minute"/>
                                    <Tab label="Transactions By Hours"/>
                                </Tabs>
                            </AppBar>
                            {txValue === 0 && <TabContainer><TxPerMins/></TabContainer>}
                            {txValue === 1 && <TabContainer><TxsPerHours/></TabContainer>}
                        </div>
                    </GridItem>
                </GridContainer>
            </div>
        );
    }
}

ChartsLayout.propTypes = {
    classes: PropTypes.object.isRequired,
};

const format = () => tick => tick;
const ValueLabel = (props) => {
    const { text } = props;
    return (
        <ValueAxis.Label
            {...props}
            text={`${text}`}
        />
    );
};

const BlockPerMinsLayout = ({ classes, blocks, getBlocksPerMin }) => (
    <>
        {
            <Chart
                data={blocks !== 0 ? blocks : emptyData}
                className={classes.chart}
                height={200}
            >
                <ArgumentAxis tickFormat={format} />
                <ValueAxis
                    max={60}
                    labelComponent={ValueLabel}
                />

                <LineSeries
                    name="Block per minute"
                    valueField="count"
                    argumentField="time"
                />
            </Chart>
        }
        <ReactInterval timeout={1000} enabled={true} callback={() => getBlocksPerMin()}/>
    </>
);

const BlockPerMins = compose(
    connect(state => ({
        blocks: path(BLOCK.DOT_BLOCKS_PER_MINS)(state)
    }),
    {
        getBlocksPerMin: fetchBlocksPerMinAC.actionCreator,
    }),
    withStyles(styles)
)(BlockPerMinsLayout);

const BlockPerHoursLayout = ({ classes, blocks, getBlocksPerHour }) => (
    <>
        <Chart
            data={blocks !== 0 ? blocks : emptyData}
            className={classes.chart}
            height={200}
        >
            <ArgumentAxis tickFormat={format} />
            <ValueAxis
                max={24}
                labelComponent={ValueLabel}
            />

            <LineSeries
                name="Block per hour"
                valueField="count"
                argumentField="time"
            />
        </Chart>
        <ReactInterval timeout={1000} enabled={true} callback={() => getBlocksPerHour()}/>
    </>
);

const BlockPerHours = compose(
    connect(state => ({
            blocks: path(BLOCK.DOT_BLOCKS_PER_HOURS)(state)
        }),
        {
            getBlocksPerHour: fetchBlocksPerHoursAC.actionCreator,
        }),
    withStyles(styles)
)(BlockPerHoursLayout);

const TxPerMinsLayout = ({ classes, txs, getTxsPerMin }) => (
    <>
        {
            <Chart
                data={txs !== 0 ? txs : emptyData}
                className={classes.chart}
                height={200}
            >
                <ArgumentAxis tickFormat={format} />
                <ValueAxis
                    max={60}
                    labelComponent={ValueLabel}
                />

                <LineSeries
                    name="Transactions per minute"
                    valueField="count"
                    argumentField="time"
                />
            </Chart>
        }
        <ReactInterval timeout={1000} enabled={true} callback={() => getTxsPerMin()}/>
    </>
);

const TxPerMins = compose(
    connect(state => ({
            txs: path(TRANSACTION.DOT_TRANSACTIONS_PER_MINS)(state)
        }),
        {
            getTxsPerMin: fetchTransactionsPerMinAC.actionCreator,
        }),
    withStyles(styles)
)(TxPerMinsLayout);

const TxPerHoursLayout = ({ classes, txs, getTxsPerHour }) => (
    <>
        <Chart
            data={txs !== 0 ? txs : emptyData}
            className={classes.chart}
            height={200}
        >
            <ArgumentAxis tickFormat={format} />
            <ValueAxis
                max={24}
                labelComponent={ValueLabel}
            />

            <LineSeries
                name="Transactions per hour"
                valueField="count"
                argumentField="time"
            />
        </Chart>
        <ReactInterval timeout={1000} enabled={true} callback={() => getTxsPerHour()}/>
    </>
);

const TxsPerHours = compose(
    connect(state => ({
            txs: path(TRANSACTION.DOT_TRANSACTIONS_PER_HOURS)(state)
        }),
        {
            getTxsPerHour: fetchTransactionsPerHoursAC.actionCreator,
        }),
    withStyles(styles)
)(TxPerHoursLayout);

export const Charts = withStyles(styles)(ChartsLayout);