import React from 'react';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { path } from 'lodash/fp';
import {
    BLOCK, TRANSACTION
} from "../../constants";
import ReactInterval from 'react-interval';

// core components
import GridItem from '../../components/Grid/GridItem.jsx';
import GridContainer from '../../components/Grid/GridContainer.jsx';
import withStyles from "@material-ui/core/styles/withStyles";
import dashboardStyle from "../../assets/jss/material-dashboard-react/views/dashboardStyle";
// view components
import {fetchBlockListAC} from "../../state/block";
import {fetchLatestTransactionsAC} from "../../state/transaction";
import Card from "../../components/Card/Card";
import CardBody from "../../components/Card/CardBody";
import Success from "../../components/Typography/Success";
import Danger from "../../components/Typography/Danger";
import TablePagination from "../../components/Table/TablePagination";
import Info from "../../components/Typography/Info";
import Button from "../../components/CustomButtons/Button";
import { Charts } from "./Charts";


export const List = ({ height, label, tableHead, tableData, tableHeaderColor, detailsUri="", hasPaging=false, getTableData=undefined }) => (
    <Card style={{ height: height, overflowY: "auto", overflowX: "auto"}}>
        <CardBody>
            { tableHeaderColor === "success" ?
                <Success style={{ marginLeft: 15, marginTop: 15 }}>
                    <b>{label}</b>
                </Success> : ''
            }
            { tableHeaderColor === "danger" ?
                <Danger style={{ marginLeft: 15, marginTop: 15 }}>
                    <b>{label}</b>
                </Danger> : ''
            }
            { tableHeaderColor === "info" ?
                <Info style={{ marginLeft: 15, marginTop: 15 }}>
                    <b>{label}</b>
                </Info> : ''
            }

            <hr style={{
                marginTop: ".75rem",
                marginBottom: ".75rem",
                opacity: ".5",
                marginLeft: "-20px",
                marginRight: "-20px"
            }}/>
            <TablePagination tableHeaderColor={tableHeaderColor}
                             tableHead={tableHead}
                             tableData={tableData}
                             page={0}
                             getTableData={getTableData}
                             isPaging={hasPaging}
                             height={height}
            />
            {
                detailsUri !== "" ?
                <div style={{ width: "70px", marginLeft: "auto", marginRight: "auto", marginTop: "30px" }}>
                    <Button color={tableHeaderColor} href={`${window.location.origin}/${detailsUri}`} size="sm">
                        More
                    </Button>
                </div> : ""
            }

        </CardBody>
    </Card>
);


export const BlocksTransactionsLayout = ({ classes, blocksList, transactionsList, getBlockList, getTransactionList }) => (
    <div>
        <GridContainer>
            <GridItem xs={6} sm={6}>
                { !blocksList || blocksList.length === 0 ? <List height={700} label={"Latest Blocks"} tableHead={["Height", "Time", "Validator", "NumTxs"]} tableData={{data: []}} tableHeaderColor={"success"}/> :
                    <List height={700} label={"Latest Blocks"} tableHead={["Height", "Time", "Validator", "NumTxs"]} tableData={blocksList} tableHeaderColor={"success"} detailsUri="blocks"/>
                }
            </GridItem>
            <GridItem xs={6} sm={6}>
                { !transactionsList || transactionsList.length === 0 ? <List height={700} label={"Latest Transactions"} tableHead={["Hash", "Height", "Time", "From"]} tableData={{data: []}} tableHeaderColor={"info"}/> :
                    <List height={700} label={"Latest Transactions"} tableHead={["Hash", "Height", "Time", "From"]} tableData={transactionsList} tableHeaderColor={"info"} detailsUri="transactions"/>
                }
            </GridItem>
        </GridContainer>
        { <ReactInterval timeout={2000} enabled={true} callback={() => getBlockList()}/> }
        { <ReactInterval timeout={2000} enabled={true} callback={() => getTransactionList()}/> }
    </div>
);

export const BlocksTransactions = compose(
    connect(
        state => ({
            blocksList: path(BLOCK.DOT_BLOCKS_LIST)(state),
            transactionsList: path(TRANSACTION.DOT_LATEST_TRANSACTIONS_LIST)(state)
        }),
        {
            getBlockList: fetchBlockListAC.actionCreator,
            getTransactionList: fetchLatestTransactionsAC.actionCreator,
        }
    ),
    withStyles(dashboardStyle)
)(BlocksTransactionsLayout);

export default withStyles(dashboardStyle)(({ classes }) =>
    (
        <div  className={classes.breakpoints}>
            <Charts />
            <BlocksTransactions />
        </div>
    )
);
