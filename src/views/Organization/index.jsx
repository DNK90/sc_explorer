import React, { useState, useEffect } from 'react';
import axios from 'axios';
import {EXPLORER_API} from "../../constants";
import {timeSince} from "../../state/block";
import {List} from "../Dashboard";
import withStyles from "@material-ui/core/styles/withStyles";


const useStyles = theme => ({
    breakpoint: {
        [theme.breakpoints.up("lg")]: {
            width: "1280px",
            marginRight: "auto",
            marginLeft: "auto"
        }
    }
});

const fetchTransactions = async (p, l, o) => {

    let { pathname } = window.location;
    if (pathname.indexOf("/organization/") > -1) {
        pathname = pathname.replace("/organization/", "");
    }
    let id = 0;
    let hash = "";
    let { data: { transactions, total, limit, page, order } } = await axios.get(`${EXPLORER_API}/api/organization/${pathname}/transactions?limit=${l}&page=${p}&order=${o}`);
    if (transactions === undefined) return {};
    let results = [];
    transactions.forEach(({ transactionHash, blockHeight, time, organizationId, idHash, method, from }) => {
        if (hash === "") {
            id = organizationId;
            hash = idHash;
        }
        results.push([
            {
                urls: [
                    {
                        url: `${window.location.origin}/transaction/${transactionHash}`,
                        text: transactionHash.slice(0, 20) + "...",
                    }
                ]
            },
            {
                urls: [
                    {
                        url: `${window.location.origin}/block/${blockHeight}`,
                        text: blockHeight,
                    }
                ]
            },
            timeSince(time.seconds),
            method,
            {
                urls: [
                    {
                        url: `${window.location.origin}/address/${from}`,
                        text: from,
                    }
                ]
            },
        ]);
    });
    return {txdata:{ data: results, total: total, limit: limit, page: page !== undefined ? page : 0, order: order !== undefined ? order : 0 }, organizationInfo:{ id: id, hash: hash }};
};

const OrganizationLayout = ({ classes }) => {

    const [organizationInfo, setOrganizationInfo] = useState({});
    const [txs, setTxs] = useState({ data: [], limit: 10, page: 0, order: 0 });

    useEffect(() => {
        const fetchTxs = async () => {
            let res = await fetchTransactions(txs.page, txs.limit, txs.order);
            setOrganizationInfo(res.organizationInfo);
            if (res.txdata === undefined || res.txdata.data === undefined || res.txdata.data.length === 0 ) {
                window.location.href = `${window.location.origin}/404`;
            }
            setTxs(res.txdata);
        };
        fetchTxs();
    }, []);

    return (
        <div className={classes.breakpoint}>
            {
                txs && txs.data.length > 0 ?
                    <List
                        height={700}
                        label={`Organization ID ${organizationInfo.id} - Hash ${organizationInfo.hash} - Page #${txs.page+1} (Found ${txs.total} transactions)`}
                        tableHead={["Hash", "Height", "Time", "Method", "From"]} tableData={txs} tableHeaderColor={"info"}
                        detailsUri="" getTableData={(page, limit, order) => {
                            const switchPage = async () => {
                                let res = await fetchTransactions(page, limit, order);
                                setTxs(res.txdata);
                            };
                            switchPage();
                        }
                    } hasPaging={true}
                    /> : ""
            }
        </div>
    )
};

export const Organization = withStyles(useStyles)(OrganizationLayout);
