import Card from '../../components/Card/Card.jsx';
import CardBody from '../../components/Card/CardBody.jsx';
import Muted from '../../components/Typography/Muted';
import Info from "../../components/Typography/Info";
import React, { useState, useEffect } from "react";
import {compose} from "recompose";
import {connect} from "react-redux";
import {path} from "lodash/fp";
import {BLOCK, EXPLORER_API} from "../../constants";
import {lifecycle} from "recompose/dist/Recompose.cjs";
import {fetchBlockListAC, toDateTime} from "../../state/block";
import {List} from "../Dashboard";
import withStyles from "@material-ui/core/styles/withStyles";
import axios from "axios";

const useStyles = theme => ({
   hrSpace: {
       marginTop: ".75rem",
       marginBottom: ".75rem",
       opacity: ".75"
   },
   block: {
       [theme.breakpoints.up("lg")]: {
           width: "1280px",
           marginRight: "auto",
           marginLeft: "auto"
       }
   }
});

const BlockLayout = ({classes}) => {

    const [block, setBlock] = useState({});

    useEffect(() => {
        const fetchBlock = async () => {
            let { pathname } = window.location;
            try {
                let { data: blockData } = await axios.get(`${EXPLORER_API}/api${pathname}`);
                if (blockData.height === undefined) window.location.href = `${window.location.origin}/404`;
                let results = [];
                if (blockData.transactions) {
                    blockData.transactions.forEach(tx => {
                        results.push([
                            {
                                urls: [
                                    {
                                        url: `${window.location.origin}/transaction/${tx.transactionHash}`,
                                        text: tx.transactionHash.slice(0, 20) + "...",
                                    }
                                ]
                            },
                            tx.organizationId ? {
                                urls: [
                                    {
                                        url: `${window.location.origin}/organization/${tx.organizationId}`,
                                        text: tx.organizationId,
                                    }
                                ]
                            } : "",
                            tx.idHash ? {
                                urls: [
                                    {
                                        url: `${window.location.origin}/organization/${tx.idHash}`,
                                        text: tx.idHash.slice(0, 20) + "...",
                                    }
                                ]
                            } : "",
                            tx.method,
                            {
                                urls: [
                                    {
                                        url: `${window.location.origin}/address/${tx.from}`,
                                        text: tx.from.slice(0, 20) + "...",
                                    }
                                ]
                            },
                            tx.nonce
                        ]);
                    })
                }
                blockData.txs = {
                    data: results,
                    total: 0,
                    limit: results.length,
                    page: 0
                };
                setBlock(blockData);
            } catch (error) {
                console.log(error.message);
                window.location.href = `${window.location.origin}/404`;
            }
        };
        fetchBlock();
    });

    return (
    <div className={classes.block}>
        <Muted><h2>Block Details</h2></Muted>
        <Card>
            <CardBody>
                <Info><b>Block Height</b></Info>
                <Muted>{block.height}</Muted>

                <hr className={classes.hrSpace} />

                <Info><b>Time</b></Info>
                <Muted>{block.time ? toDateTime(block.time.seconds - (new Date().getTimezoneOffset() * 60)).toLocaleString() : 0}</Muted>

                <hr className={classes.hrSpace} />

                <Info><b>Validator</b></Info>
                <Muted>{block.validator}</Muted>

                <hr className={classes.hrSpace} />

                <Info><b>Number Of Transactions</b></Info>
                <Muted>{block.numTxs !== undefined ? block.numTxs : 0}</Muted>
            </CardBody>
        </Card>
        { block.txs !== undefined && block.txs.data.length > 0 ?
            <List height={400} label={"Transactions"} tableHead={["Hash", "Organization ID", "Organization Hash", "Method", "From", "Nonce"]} tableData={block.txs} tableHeaderColor={"info"} detailsUri=""/>
            : ""
        }
    </div>
)};

const BlocksLayout = ({ classes, blocks, getBlocks }) => (
    <div className={classes.block}>
        {   // let label =
            blocks !== undefined && blocks.data !== undefined && blocks.data.length > 0 ?
            <List
                height={700}
                label={`Block #${blocks.blocks[0].height} to #${blocks.blocks[blocks.blocks.length - 1].height} (Total of ${blocks.total} blocks)`}
                tableHead={["Height", "Time", "Validator", "NumTxs"]} tableData={blocks} tableHeaderColor={"info"}
                detailsUri="" getTableData={getBlocks} hasPaging={true}
            /> : ""
        }
    </div>
);

export const Block = withStyles(useStyles)(BlockLayout);

export const Blocks = compose(
    connect(
        state => ({
            blocks: path(BLOCK.DOT_BLOCKS_LIST)(state)
        }),
        {
            getBlocks: fetchBlockListAC.actionCreator
        }
    ),
    withStyles(useStyles),
    lifecycle({
        componentDidMount() {
            this.props.getBlocks()
        }
    })
)(BlocksLayout);
