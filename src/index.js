import React from "react";
import { render } from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch } from "react-router-dom";
import "assets/css/material-dashboard-react.css?v=1.5.0";
import configureStore from "store/configStore";
import indexRoutes from "routes/index.jsx";
import { Provider } from "react-redux";

export const hist = createBrowserHistory();

const store = configureStore();
store.subscribe(() => {
  // console.log(store.getState());
});

const App = () => (
  <Provider store={store}>
    <Router history={hist}>
      <Switch>
        {indexRoutes.map((prop, key) => {
          return (
            <Route path={prop.path} component={prop.component} key={key} />
          );
        })}
      </Switch>
    </Router>
  </Provider>
);

render(<App />, document.getElementById("root"));
