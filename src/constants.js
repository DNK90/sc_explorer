export const EXPLORER_API = process.env["REACT_APP_EXPLORER"] ? process.env["REACT_APP_EXPLORER"] : "http://localhost:8080";

export const GET_BLOCK_LIST = "getBlockList";
export const GET_BLOCKS_PER_MINS = 'getBlocksPerMins';
export const GET_BLOCKS_PER_HOURS = "getBlocksPerHours";
export const GET_BLOCK_LIST_PAGE = "getBlockListPage";
export const GET_BLOCK = "getBlock";
export const GET_TRANSACTIONS_LIST = "getTransactionsList";
export const GET_LATEST_TRANSACTION_LIST = "getLatestTransactionList";
export const GET_TRANSACTION = "getTransaction";
export const GET_TRANSACTIONS_PER_MINS = "getTransactionsPerMins";
export const GET_TRANSACTIONS_PER_HOURS = "getTransactionsPerHours";

export const BLOCK = {
  BLOCKS_LIST: "block/list",
  DOT_BLOCKS_LIST: "block.list",
  BLOCK_INFO: "block/info",
  DOT_BLOCK_INFO: "block.info",
  BLOCKS_PER_MINS: "block/blocksPerMins",
  DOT_BLOCKS_PER_MINS: "block.blocksPerMins",
  BLOCKS_PER_HOURS: "block/blocksPerHours",
  DOT_BLOCKS_PER_HOURS: "block.blocksPerHours"
};

export const TRANSACTION = {
  LATEST_TRANSACTIONS_LIST: "transaction/latest",
  DOT_LATEST_TRANSACTIONS_LIST: "transaction.latest",
  TRANSACTION_INFO: "transaction/info",
  DOT_TRANSACTION_INFO: "transaction.info",
  TRANSACTIONS_LIST: "transaction/list",
  DOT_TRANSACTIONS_LIST: "transaction.list",
  OPEN_DECODE_MODAL: "transaction/openDecodeModal",
  DOT_OPEN_DECODE_MODAL: "transaction.openDecodeModal",
  PROTO: "transaction/proto",
  DOT_PROTO: "transaction.proto",
  PROTO_TYPE: "transaction/protoType",
  DOT_PROTO_TYPE: "transaction.protoType",
  PROTO_TYPES: "transaction/protoTypes",
  DOT_PROTO_TYPES: "transaction.protoTypes",
  DECODE_RESULT: "transaction/decodeResult",
  DOT_DECODE_RESULT: "transaction.decodeResult",
  TRANSACTIONS_PER_MINS: "transaction/transactionsPerMins",
  DOT_TRANSACTIONS_PER_MINS: "transaction.transactionsPerMins",
  TRANSACTIONS_PER_HOURS: "transaction/transactionsPerHours",
  DOT_TRANSACTIONS_PER_HOURS: "transaction.transactionsPerHours"
};
