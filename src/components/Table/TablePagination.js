import React from "react";
import PropTypes from "prop-types";
import { compose, defaultProps, setPropTypes } from "recompose";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
// core components
import tableStyle from "assets/jss/material-dashboard-react/components/tableStyle.jsx";
import Button from '../CustomButtons/Button.jsx';
import TableContainer from '@material-ui/core/TableContainer';

export const Link = ({urls}) => {
  if (urls.length > 1)
    return (
      <div style={{ overflowY: "auto", msOverflowY: "auto", maxHeight: 50, height: 25 }}>
        {
          urls.map((prop, key) => {
            return (
              <p style={{ marginTop: 10 }}><a rel={"noopener noreferrer"} href={prop.url}>{prop.text}</a></p>
            )
          })
        }
      </div>
    );

  return (
    <a rel={"noopener noreferrer"} href={urls[0].url}>{urls[0].text}</a>
  );

};

const TablePaginationLayout = ({ classes, tableHead, tableData, tableHeaderColor, getTableData, isPaging, height }) => (
  <div className={classes.tableResponsive}>
    <TableContainer style={{ maxHeight: height-150 }}>
      <Table stickyHeader className={classes.table}>
        {tableHead !== undefined ? (
          <TableHead className={classes[tableHeaderColor + "TableHeader"]}>
            <TableRow>
              {tableHead.map((prop, key) => {
                return (
                  <TableCell
                    className={classes.tableCell + " " + classes.tableHeadCell}
                    key={key}
                  >
                    {prop}
                  </TableCell>
                );
              })}
            </TableRow>
          </TableHead>
        ) : null}
        {(tableData.data !== null && tableData.data !== undefined && tableData.data.length !== 0) ?
          <TableBody>
            {
              tableData.data.map((prop, key) => {
                return (
                  <TableRow key={key}>
                    {prop.map((prop, key) => {
                      return (
                        <TableCell className={classes.tableCell} key={key}>
                          {
                            typeof prop === 'object' && prop["urls"] !== undefined ?
                              <Link urls={prop["urls"]}/> : prop
                          }
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })
            }
          </TableBody>
        : null}
      </Table>
    </TableContainer>
    { tableData.data === null || tableData.data === undefined || tableData.data.length === 0 ? <p>No data found.</p> : ''}
    {
      isPaging ?
          <table style={{ border: "None", width: "100%", position: "absolute", right: 20, bottom: 10 }}>
            <tbody>
            <tr>
              <td/>
              <td align={"right"}>
                { tableData.page > 0 ?
                    <Button onClick={() => {
                      getTableData(tableData.page-1, tableData.limit, tableData.order)
                    }} variant="contained" color="success" size="sm">
                      Previous
                    </Button> : ''
                }
                { tableData.page >= 0 && tableData.page < calculateTotalPage(tableData) ?
                    <Button onClick={() => {
                      console.log(getTableData);
                      getTableData(tableData.page+1, tableData.limit, tableData.order);
                    }} variant="contained" color="success" size="sm">
                      Next
                    </Button> : ''
                }
              </td>
            </tr>
            </tbody>
          </table>
          : ""
    }
  </div>
);

const calculateTotalPage = (tableData) => {
  if (tableData.total === 0 || tableData.limit === 0) return 0;
  else if (tableData.total <= tableData.limit) return 0; // page 0 is the first page
  let totalPage = Math.ceil(tableData.total / tableData.limit);
  return totalPage === 1 ? 0 : totalPage-1;
};

export default compose(
  defaultProps({
    tableHeaderColor: "gray",
    length: 5,
  }),
  setPropTypes({
    classes: PropTypes.object,
    tableHeaderColor: PropTypes.oneOf([
      "warning",
      "primary",
      "danger",
      "success",
      "info",
      "rose",
      "gray"
    ]),
    tableHead: PropTypes.arrayOf(PropTypes.string),
    tableData: PropTypes.object
  }),
  withStyles(tableStyle)
)(TablePaginationLayout);

