import React, { useState, useEffect } from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// @material-ui/icons
import Search from "@material-ui/icons/Search";
// core components
import CustomInput from "../../components/CustomInput/CustomInput.jsx";
import Button from "../../components/CustomButtons/Button.jsx";

import headerLinksStyle from "../../assets/jss/material-dashboard-react/components/headerLinksStyle.jsx";
import Muted from "../Typography/Muted";
import Select from '@material-ui/core/Select';
import MenuItem from "@material-ui/core/MenuItem";

const searchByOrganization = "Enter Organization Hash or Organization ID";
const searchByTransaction = "Enter Transaction Hash";
const searchByAddress = "Enter Address";
const searchByBlock = "Enter Block Height";


const HeaderLinks = ({ classes }) => {
  const [searchValue, setSearchValue] = useState("");
  const [searchType, setSearchType] = useState(1);
  const [placeHolder, setPlaceHolder] = useState(searchByBlock);

  const changePlaceHolder = (type) => {
    switch (type) {
      case 2: setPlaceHolder(searchByTransaction); break;
      case 3: setPlaceHolder(searchByOrganization); break;
      case 4: setPlaceHolder(searchByAddress); break;
      case 1:
      default: setPlaceHolder(searchByBlock)
    }
  };

  const handleSearchClick = () => {
    switch(searchType) {
      case 2: window.location.href = `/transaction/${searchValue}`; break;
      case 3: window.location.href = `/organization/${searchValue}`; break;
      case 4: window.location.href = `/address/${searchValue}`; break;
      case 1:
      default: window.location.href = `/block/${searchValue}`;
    }
  };

  useEffect(() => {
    let history = [];
    if (localStorage.getItem("history")) {
      history = JSON.parse(localStorage.getItem("history"));
    }
    if (history.length > 0 && history[history.length-1] === window.location.href) return;
    if (window.location.href.indexOf("#back") < 0) {
      history.push(window.location.href);
      localStorage.setItem("history", JSON.stringify(history));
    }
  });

  return (
    <div style={{ position: "relative", display: "flex", alignItems: "center", height: "60px", background: "#fff", boxShadow: "0 1px 4px rgba(0, 21, 41, .08)", paddingBottom: "30px" }}>
      <div className={classes.logo} style={{ position: "absolute", left: 50, top: 15, display: "inline-flex", width: "400px" }}>
        <a href="/" className={classes.logoLink}>
          <div className={classes.logoImage}>
            <img src={`${window.location.origin}/favicon.ico`} alt="logo" className={classes.img} style={{ width: "50px" }}/>
          </div>
        </a>
        <div style={{ width: "320px", marginLeft: "30px" }}>
          <a href="/">
            <Muted><h4 style={{ marginTop: 15 }}><b>Lina Network - Blockchain Explorer</b></h4></Muted>
          </a>
        </div>
      </div>
      <div className={classes.searchWrapper} style={{ width: 600, top: 30, position: "absolute", right: 0 }}>
        <Select
            variant="standard"
            value={searchType}
            onChange={(evt) => {
              setSearchType(evt.target.value);
              changePlaceHolder(evt.target.value);
            }}
            style={{ width: 150, marginRight: 15 }}
        >
          <MenuItem value={1}>Block</MenuItem>
          <MenuItem value={2}>Transaction</MenuItem>
          <MenuItem value={3}>Organization</MenuItem>
          <MenuItem value={4}>Address</MenuItem>
        </Select>
        <CustomInput
          formControlProps={{
            className: classes.margin + " " + classes.search
          }}
          inputProps={{
            placeholder: `${placeHolder}`,
            inputProps: {
              "aria-label": `${placeHolder}`,
              style: { width: "300px", marginRight: 15, marginTop: 10 }
            },
            onChange: (evt) => {
              setSearchValue(evt.target.value);
            }
          }}
        />
        <Button color="white" aria-label="edit" justIcon round style={{ marginLeft: 20, marginBottom: 10 }} onClick={() => handleSearchClick()}>
          <Search />
        </Button>
      </div>
    </div>
  );
};

export default withStyles(headerLinksStyle)(HeaderLinks);
