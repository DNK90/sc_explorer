import thunk from 'redux-thunk';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { reducers as apiReducers } from 'redux-api-call';
import { reducer as formReducer } from 'redux-form';
import { createEpicMiddleware } from 'redux-observable';
import {
    createAPIMiddleware,
    defaultTransformers,
    composeAdapters,
} from 'redux-api-call';
import { blockReducers } from "../state/block";
import { transactionReducers } from "../state/transaction";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const epicMiddleware = createEpicMiddleware();

const apiMiddleware = createAPIMiddleware(
    composeAdapters(...[...defaultTransformers]),
);

export default () => {
  return createStore(
    combineReducers({
      ...apiReducers,
      form: formReducer,
      block: blockReducers,
      transaction: transactionReducers
    }),
    composeEnhancers(applyMiddleware(thunk, epicMiddleware, apiMiddleware)),
  );
};
