// @material-ui/icons
import HomeIcon from "@material-ui/icons/Home";
import DashboardIcon from "@material-ui/icons/Dashboard";
// import ContentPaste from "@material-ui/icons/ContentPaste";
// core components/views
import Dashboard from "../views/Dashboard";
import { Transaction, Transactions } from "../views/Transactions";
import { Block, Blocks } from "../views/Blocks";
import { Organization } from "../views/Organization";
import { Address } from "../views/Address";
import { NotFound } from "../views/Dashboard/404";


const dashboardRoutes = [
  {
    path: "/home",
    sidebarName: "Home",
    navbarName: "Home",
    icon: HomeIcon,
    component: Dashboard
  },
  {
    path: "/transaction",
    sidebarName: "Transaction",
    navbarName: "Transaction",
    icon: DashboardIcon,
    component: Transaction
  },
  {
    path: "/block",
    sidebarName: "Block",
    navbarName: "Block",
    icon: DashboardIcon,
    component: Block
  },
  {
    path: "/blocks",
    sidebarName: "Blocks",
    navbarName: "Blocks",
    icon: DashboardIcon,
    component: Blocks
  },
  {
    path: "/transactions",
    sidebarName: "Transactions",
    navbarName: "Transactions",
    icon: DashboardIcon,
    component: Transactions
  },
  {
    path: "/organization",
    sidebarName: "Organization",
    navbarName: "Organization",
    icon: DashboardIcon,
    component: Organization
  },
  {
    path: "/address",
    sidebarName: "Address",
    navbarName: "Address",
    icon: DashboardIcon,
    component: Address
  },
  {
    path: "/404",
    sidebarName: "NotFound",
    navbarName: "NotFound",
    icon: DashboardIcon,
    component: NotFound
  },
  { redirect: true, path: "/", to: "/home", navbarName: "Redirect" },
];

export default dashboardRoutes;
