import { ACTIONS, makeFetchAction } from 'redux-api-call';
import { handleActions } from 'redux-actions';
import { path } from 'lodash/fp';
import { combineReducers } from "redux";
import {EXPLORER_API, GET_BLOCK_LIST, GET_BLOCKS_PER_HOURS, GET_BLOCKS_PER_MINS} from "../constants";

export const toDateTime = (secs) => {
    let t = new Date(1970, 0, 1); // Epoch
    t.setSeconds(secs);
    return t;
};

function calculateTime(seconds) {
    if (seconds <= 0) return "";
    if (seconds < 60) {
        return seconds + ' seconds';
    }
    if (seconds < 3600) {
        let min = Math.floor(seconds / 60);
        return min + ' minutes ' + calculateTime(Math.floor(seconds%60));
    }
    let hours = Math.floor(seconds / 3600);
    return hours + ' hours';
}

export const timeSince = (seconds) => {
    let now = new Date(),
        secondsPast = Math.floor(now.getTime()/1000) - seconds;
    if (secondsPast <= 86400) return calculateTime(secondsPast);
    return toDateTime(seconds).toLocaleDateString("en-GB");
};

export const fetchBlockListAC = makeFetchAction(GET_BLOCK_LIST, (page=undefined, limit=undefined, order=undefined) => ({
    endpoint: () => {
        let url = `${EXPLORER_API}/api/blocks?limit=${limit !== undefined ? limit : 10}`;
        url += `&page=${page === undefined ? 0 : page}`;
        if (order === undefined || order !== 1) order = 0;
        url += `&order=${order}`;
        return url;
    }
}));

export const fetchBlocksPerMinAC = makeFetchAction(GET_BLOCKS_PER_MINS, () => ({
    endpoint: `${EXPLORER_API}/api/blocksbyMinute`
}));

export const fetchBlocksPerHoursAC = makeFetchAction(GET_BLOCKS_PER_HOURS, () => ({
    endpoint: `${EXPLORER_API}/api/blocksbyHour`
}));

const getBlockTableData = data => {
  let results = [];
  data.forEach(el => {
      results.push([
          {
              urls: [
                  {
                      url: `${window.location.origin}/block/${el.height}`,
                      text: el.height,
                  }
              ]
          },
          timeSince(el.time.seconds),
          el.validator,
          el.numTxs ? el.numTxs : 0
      ]);
  });
  return results;
};

export const blockListReducer = handleActions({
    [ACTIONS.COMPLETE]: (state, { payload }) => {
        if (path('name', payload) === GET_BLOCK_LIST) {
            let data = path("json", payload);
            if (data.page === undefined) data.page = 0;
            let results = getBlockTableData(data.blocks);
            return {
                ...data,
                data: results
            };
        }
        return state;
    },
    [ACTIONS.FAILURE]: (state, { payload }) => {
        if (payload.name === GET_BLOCK_LIST) {
            console.log(`handle getBlockList failed ${JSON.stringify(payload)}`);
        }
        return state;
    }
}, 0);

export const blockPerMinsReducer = handleActions({
    [ACTIONS.COMPLETE]: (state, { payload }) => {
        if (path('name', payload) === GET_BLOCKS_PER_MINS) {
            let data = path("json", payload);
            let results = [];
            let count = 0;
            if (data.length > 0) {
                data.forEach(el => {
                    results.push({
                        time: count+1,
                        count: el.count
                    });
                    count++;
                })
            }
            return results;
        }
        return state;
    },
    [ACTIONS.FAILURE]: (state, { payload }) => {
        if (payload.name === GET_BLOCKS_PER_MINS) {
            console.log(`handle blockPerMins failed ${JSON.stringify(payload)}`);
        }
        return state;
    }
}, 0);

export const blockPerHoursReducer = handleActions({
    [ACTIONS.COMPLETE]: (state, { payload }) => {
        if (path('name', payload) === GET_BLOCKS_PER_HOURS) {
            let data = path("json", payload);
            let results = [];
            let count = 0;
            if (data.length > 0) {
                data.forEach(el => {
                    results.push({
                        time: count+1,
                        count: el.count
                    });
                    count++;
                })
            }
            return results;
        }
        return state;
    },
    [ACTIONS.FAILURE]: (state, { payload }) => {
        if (payload.name === GET_BLOCKS_PER_HOURS) {
            console.log(`handle blockPerHours failed ${JSON.stringify(payload)}`);
        }
        return state;
    }
}, 0);

export const blockReducers = combineReducers({
    list: blockListReducer,
    blocksPerMins: blockPerMinsReducer,
    blocksPerHours: blockPerHoursReducer
});