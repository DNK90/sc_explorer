import { ACTIONS, makeFetchAction } from 'redux-api-call';
import { handleActions } from 'redux-actions';
import { path } from 'lodash/fp';
import { combineReducers } from "redux";
import {
    EXPLORER_API,
    GET_LATEST_TRANSACTION_LIST,
    GET_TRANSACTION,
    GET_TRANSACTIONS_LIST, GET_TRANSACTIONS_PER_HOURS, GET_TRANSACTIONS_PER_MINS,
} from "../constants";
import { timeSince } from "./block";

export const fetchLatestTransactionsAC = makeFetchAction(GET_LATEST_TRANSACTION_LIST, (page, limit, order) => ({
    endpoint: () => {
        let url = `${EXPLORER_API}/api/transactions?limit=${limit > 0 ? limit : 10}`;
        if (page && page !== "") url += `&page=${page}`;
        if (order !== 1) order = 0;
        url += `&order=${order}`;
        return url;
    }
}));

export const fetchTransactionInfoAC = makeFetchAction(GET_TRANSACTION, () => {
    let { pathname } = window.location;
    if (pathname.indexOf("transaction/") > -1) {
        pathname = pathname.replace("transaction/", "");
    }
    return {
        endpoint: `${EXPLORER_API}/api/transaction/${pathname}`
    }
});

export const fetchTransactionsAC = makeFetchAction(GET_TRANSACTIONS_LIST, (page, limit, order) => ({
    endpoint: () => {
        let url = `${EXPLORER_API}/api/transactions?limit=${limit > 0 ? limit : 10}`;
        if (page && page !== "") url += `&page=${page}`;
        if (order !== 1) order = 0;
        url += `&order=${order}`;
        return url;
    }
}));

export const latestTransactionsReducer = handleActions({
    [ACTIONS.COMPLETE]: (state, { payload }) => {
        if (path('name', payload) === GET_LATEST_TRANSACTION_LIST) {
            let txData = path("json", payload);
            if (txData.page === undefined) txData.page = 0;
            let results = [];
            txData.transactions.forEach(tx => {
                results.push([
                    {
                        urls: [
                            {
                                url: `${window.location.origin}/transaction/${tx.transactionHash}`,
                                text: tx.transactionHash.slice(0, 10) + "...",
                            }
                        ]
                    },
                    {
                        urls: [
                            {
                                url: `${window.location.origin}/block/${tx.blockHeight}`,
                                text: tx.blockHeight,
                            }
                        ]
                    },
                    timeSince(tx.time.seconds),
                    {
                        urls: [
                            {
                                url: `${window.location.origin}/address/${tx.from}`,
                                text: tx.from.slice(0, 10) + "...",
                            }
                        ]
                    },
                ]);
            });
            return {
                ...txData,
                data: results,
            };
        }
        return state;
    },
    [ACTIONS.FAILURE]: (state, { payload }) => {
        if (payload.name === GET_LATEST_TRANSACTION_LIST) {
            console.log(`handle getTransactionList failed ${JSON.stringify(payload)}`);
        }
        return state;
    }
}, 0);

export const transactionsReducer = handleActions({
    [ACTIONS.COMPLETE]: (state, { payload }) => {
        if (path('name', payload) === GET_TRANSACTIONS_LIST) {
            let txData = path("json", payload);
            if (txData.page === undefined) txData.page = 0;
            let results = [];
            txData.transactions.forEach(tx => {
                results.push([
                    {
                        urls: [
                            {
                                url: `${window.location.origin}/transaction/${tx.transactionHash}`,
                                text: tx.transactionHash.slice(0, 20) + "...",
                            }
                        ]
                    },
                    {
                        urls: [
                            {
                                url: `${window.location.origin}/block/${tx.blockHeight}`,
                                text: tx.blockHeight,
                            }
                        ]
                    },
                    timeSince(tx.time.seconds),
                    tx.organizationId ? {
                        urls: [
                            {
                                url: `${window.location.origin}/organization/${tx.organizationId}`,
                                text: tx.organizationId,
                            }
                        ]
                    } : "",
                    tx.idHash ? {
                        urls: [
                            {
                                url: `${window.location.origin}/organization/${tx.idHash}`,
                                text: tx.idHash.slice(0, 20) + "...",
                            }
                        ]
                    } : "",
                    tx.method,
                    {
                        urls: [
                            {
                                url: `${window.location.origin}/address/${tx.from}`,
                                text: tx.from,
                            }
                        ]
                    },
                ]);
            });
            return {
                ...txData,
                data: results,
            };
        }
        return state;
    },
    [ACTIONS.FAILURE]: (state, { payload }) => {
        if (payload.name === GET_TRANSACTIONS_LIST) {
            console.log(`handle getTransactionList failed ${JSON.stringify(payload)}`);
        }
        return state;
    }
}, 0);

export const transactionReducer = handleActions({
    [ACTIONS.COMPLETE]: (state, { payload }) => {
        if (path('name', payload) === GET_TRANSACTION) {
            return path("json", payload);
        }
        return state;
    },
    [ACTIONS.FAILURE]: (state, { payload }) => {
        if (payload.name === GET_TRANSACTION) {
            console.log(`handle getTransactionList failed ${JSON.stringify(payload)}`);
        }
        return state;
    }
}, 0);

export const fetchTransactionsPerMinAC = makeFetchAction(GET_TRANSACTIONS_PER_MINS, () => ({
    endpoint: `${EXPLORER_API}/api/txByMinute`
}));

export const fetchTransactionsPerHoursAC = makeFetchAction(GET_TRANSACTIONS_PER_HOURS, () => ({
    endpoint: `${EXPLORER_API}/api/txByHour`
}));

export const transactionPerMinsReducer = handleActions({
    [ACTIONS.COMPLETE]: (state, { payload }) => {
        if (path('name', payload) === GET_TRANSACTIONS_PER_MINS) {
            let data = path("json", payload);
            let results = [];
            let count = 0;
            if (data.length > 0) {
                data.forEach(el => {
                    results.push({
                        time: count+1,
                        count: el.count
                    });
                    count++;
                })
            }
            return results;
        }
        return state;
    },
    [ACTIONS.FAILURE]: (state, { payload }) => {
        if (payload.name === GET_TRANSACTIONS_PER_MINS) {
            console.log(`handle txPerMins failed ${JSON.stringify(payload)}`);
        }
        return state;
    }
}, 0);

export const transactionPerHoursReducer = handleActions({
    [ACTIONS.COMPLETE]: (state, { payload }) => {
        if (path('name', payload) === GET_TRANSACTIONS_PER_HOURS) {
            console.log("get txs per hours");
            let data = path("json", payload);
            let results = [];
            let count = 0;
            if (data.length > 0) {
                data.forEach(el => {
                    results.push({
                        time: count+1,
                        count: el.count
                    });
                    count++;
                })
            }
            return results;
        }
        return state;
    },
    [ACTIONS.FAILURE]: (state, { payload }) => {
        if (payload.name === GET_TRANSACTIONS_PER_HOURS) {
            console.log(`handle txPerHours failed ${JSON.stringify(payload)}`);
        }
        return state;
    }
}, 0);

export const transactionReducers = combineReducers({
    latest: latestTransactionsReducer,
    info: transactionReducer,
    list: transactionsReducer,
    transactionsPerMins: transactionPerMinsReducer,
    transactionsPerHours: transactionPerHoursReducer,
});